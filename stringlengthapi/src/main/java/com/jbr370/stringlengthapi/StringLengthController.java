package com.jbr370.stringlengthapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class StringLengthController {
    @CrossOrigin
    @GetMapping("/length")
    public int getLength(@RequestParam(required=true) String text) {
        int result = text.length();
        return result;
    }
    
}
